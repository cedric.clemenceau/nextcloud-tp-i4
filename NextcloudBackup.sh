#!/bin/bash

# Variables
backupMainDir="/var/backups/nextcloud"

echo "Backup directory: $backupMainDir"

currentDate=$(date +"%Y%m%d_%H%M%S")

# The actual directory of the current backup - this is a subdirectory of the main directory above with a timestamp
backupdir="${backupMainDir}/${currentDate}/"

nextcloudDataDir="/var/www/html/nextcloud"

webserverServiceName="apache2"

nextcloudDatabase="nextcloud"

# Use your own db user
dbUser="root"

# Use your own db password
dbPassword="root"

# Use your own webserver user
webserverUser="www-data"

# For one backup per day we stay at 30 backup max
maxNrOfBackups=30

fileNameBackupDataDir="nextcloud-datadir.tar.gz"

fileNameBackupDb="nextcloud-db.sql"

function DisableMaintenanceMode() {
	sudo -u "${webserverUser}" php ${nextcloudDataDir}/occ maintenance:mode --off
	echo
}

#
# Check if backup dir already exists
#
if [ ! -d "${backupdir}" ]
then
	mkdir -p "${backupdir}"
else
	echo "This backup folder already exist"
fi

#
# Set maintenance mode
#
sudo -u "${webserverUser}" php ${nextcloudDataDir}/occ maintenance:mode --on
echo

#
# Stop web server
#
systemctl stop "${webserverServiceName}"
echo

#
# Backup data directory
#
tar -cpzf "${backupdir}/${fileNameBackupDataDir}"  -C "${nextcloudDataDir}" .
echo

echo "Backup Nextcloud database..."
# MySQL/MariaDB:
mysqldump --single-transaction -h localhost -u "${dbUser}" -p"${dbPassword}" "${nextcloudDatabase}" > "${backupdir}/${fileNameBackupDb}"


#
# Start web server
#
echo "Starting web server..."
systemctl start "${webserverServiceName}"
echo "Done"
echo

#
# Disable maintenance mode
#
DisableMaintenanceMode

#
# Delete old backups
#
if (( ${maxNrOfBackups} != 0 ))
then
	nrOfBackups=$(ls -l ${backupMainDir} | grep -c ^d)

	if (( ${nrOfBackups} > ${maxNrOfBackups} ))
	then
		echo "Removing old backups..."
		ls -t ${backupMainDir} | tail -$(( nrOfBackups - maxNrOfBackups )) | while read dirToRemove; do
			echo "${dirToRemove}"
			rm -r ${backupMainDir}/${dirToRemove}
			echo "Done"
			echo
		done
	fi
fi

echo
echo "DONE!"
echo "Backup created: ${backupdir}"
