#!/bin/bash

# Variables
restore=$1
backupMainDir=$2

if [ -z "$backupMainDir" ]; then
    backupMainDir="/var/backups/nextcloud/"
fi

echo "Backup directory: $backupMainDir"

currentRestoreDir="${backupMainDir}/${restore}"

nextcloudDataDir="/var/www/html/nextcloud"

webserverServiceName="apache2"

nextcloudDatabase="nextcloud"

dbUser="root"

dbPassword="root"

webserverUser="www-data"

fileNameBackupDataDir="nextcloud-datadir.tar.gz"

fileNameBackupDb="nextcloud-db.sql"

# Check if parameter(s) given
#
if [ $# != "1" ] && [ $# != "2" ]
then
    echo "ERROR: No backup name to restore given, or wrong number of parameters!"
    echo "Usage: NextcloudRestore.sh 'BackupDate' ['BackupDirectory']"
    exit 1
fi

# Check if backup dir exists
#
if [ ! -d "${currentRestoreDir}" ]
then
	 echo "ERROR: Backup ${restore} not found!"
    exit 1
fi

#
# Set maintenance mode
#
# ssh 192.168.33.200 "sudo -u ..."
sudo -u "${webserverUser}" php "${nextcloudDataDir}"/occ maintenance:mode --on
echo

#
# Stop web server
#
# ssh 192.168.33.200 "systemctl ..." 
echo "Stopping web server..."
systemctl stop "${webserverServiceName}"
echo "Done"
echo

#
# Delete old Nextcloud direcories
#

# Data directory
# ssh 192.168.33.200 "rm -r "${}" 
# ssh 192.168.33.200 "mkdir -p "${}"
echo "Deleting old Nextcloud data directory..."
rm -r "${nextcloudDataDir}"
mkdir -p "${nextcloudDataDir}"
echo "Done"
echo

#
# Restore file and data directory
#

# Data directory
echo "Restoring Nextcloud data directory..."
tar -xmpzf "${currentRestoreDir}/${fileNameBackupDataDir}" -C "${nextcloudDataDir}"
echo "Done"
echo

#
# Restore database
#
echo "Dropping old Nextcloud DB..."
# MySQL/MariaDB:
# ssh 192.168.33.200 "mysql -h ..."
mysql -h localhost -u "${dbUser}" -p"${dbPassword}" -e "DROP DATABASE ${nextcloudDatabase}"

echo "Creating new DB for Nextcloud..."
# MySQL/MariaDB:
# ssh 192.168.33.200 "mysql -h ..."
mysql -h localhost -u "${dbUser}" -p"${dbPassword}" -e "CREATE DATABASE ${nextcloudDatabase} CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci"

echo "Restoring backup DB..."
# MySQL/MariaDB:
# ssh 192.168.33.200 "mysql -h ..."
mysql -h localhost -u "${dbUser}" -p"${dbPassword}" "${nextcloudDatabase}" < "${currentRestoreDir}/${fileNameBackupDb}"

#
# Start web server
#
echo "Starting web server..."
systemctl start "${webserverServiceName}"
echo "Done"
echo

#
# Set directory permissions
#
echo "Setting directory permissions..."
chown -R "${webserverUser}":"${webserverUser}" "${nextcloudDataDir}"
echo "Done"
echo

echo "Updating the system data-fingerprint..."
# ssh 192.168.33.200 "sudo ... -u"
sudo -u "${webserverUser}" php ${nextcloudDataDir}/occ maintenance:data-fingerprint
echo "Done"
echo

#
# Disbale maintenance mode
#
echo "Switching off maintenance mode..."
# ssh 192.168.33.200 "sudo ... -u"
sudo -u "${webserverUser}" php ${nextcloudDataDir}/occ maintenance:mode --off
echo "Done"
echo

echo
echo "DONE!"
echo "Backup ${restore} successfully restored."
